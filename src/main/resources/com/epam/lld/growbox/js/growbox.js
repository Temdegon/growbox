var devs = ["light", "fan", "pump"];
var stats = ["temp", "hum"];

$(document).ready(function() {
  updateStatus();
  initControls();

  $("#time-lapse").click(function(){
      $.get("/time-lapse", function(data) {
        var jsData = jQuery.parseJSON(data);
        preload(jsData);
        var lastSlide = jsData[jsData.length-1];
        
        $('#cam').hide();
        $('#slide-show').show();        
      
        $('.fadein img:gt(0)').hide();
        slideShowIntervalId = setInterval(function(){
          if ($('.fadein :first-child').get(0).src.indexOf(lastSlide) != -1) {
            clearInterval(slideShowIntervalId);
                  $('#slide-show').hide();        
                  $('#cam').show();
                  return;
          }
          $('.fadein :first-child').hide()
             .next('img').show()
             .end().appendTo('.fadein');}, 
          500);
        });
      
    });
    
    startStatusUpdate();
  });
 
function initControls() {
  $.each(devs, function(index, value) {
    $("#" + value + "Switch").click(function() {
      $.post("/control", {id: value}, function() {
        var el = $('#' + value + 'Status');
        el.prop('checked', !el.prop('checked'))
      });
    });
  });
  
  $("#camButton").click(function() {
    $('#photo-loader').css('display', 'block');
    $.post("/control", {id: "camButton"}, function(newPhoto) {
      updatePhoto(newPhoto);
      $("#cam").show();
    });
  });  
}

function startStatusUpdate() {
  console.log("Starting status update");
  statusIntervalId = setInterval(function() {
    updateStatus();
  }, 2000);  
}

function stopStatusUpdate() {
  console.log("Stop status update")
  clearInterval(statusIntervalId);
}

function preload(arrayOfImages) {
    var sliderDiv = $('#slide-show');
    sliderDiv.empty();
    $(arrayOfImages).each(function(){
      sliderDiv.append("<img  src='" + this +"' />");
    });
}

function updatePhoto(photo) {
  $('#photo-loader').css('display', 'none');
  $("#cam").css('background-image', 'url(' + photo + ')');
}

function toggleSettings() {
    var settings = $('#settings');
    var settingsItem = $('#settings-item');
    settings.toggleClass('active');
}

function updateStatus() {
  $.get("/status", function(data) {
    var jsData = jQuery.parseJSON(data);
    if ($("#cam").css('background-image') &&
            $("#cam").css('background-image').indexOf(jsData['img']) == -1) {
      $("#cam").css('background-image', 'url(' + jsData['img'] + ')');
    }
    $.each(devs, function(index, value) {
      $('#' + value + 'Status').prop('checked', jsData[value] == "true");
    });
    $.each(stats, function(index, key) {
      $("#" + key + "Status").html(jsData[key]);
    });

  });
}

function stringValue(key, value) {
  if (devs.indexOf(key) != -1) {
    return (value ? "On" : "Off");
  }
  return value;
}

function toggleButton(buttonId) {
  $.post("/control", {id: buttonId}, function() {
    updateStatus();
  });
}