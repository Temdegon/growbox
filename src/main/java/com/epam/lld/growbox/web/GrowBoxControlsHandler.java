package com.epam.lld.growbox.web;

import com.epam.lld.growbox.GrowBox;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jackson.map.ObjectMapper;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

/**
 *
 * @author Pavel_Vervenko
 */
public class GrowBoxControlsHandler extends AbstractHandler {

    private static final String PATH = "/control";
    private final GrowBox box;
    private final ObjectMapper jsonMapper = new ObjectMapper();

    GrowBoxControlsHandler(GrowBox newBox) {
        this.box = newBox;
    }

    @Override
    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if (target.startsWith("/status")) {
            jsonMapper.writeValue(response.getWriter(), box.getStatus());
            response.setStatus(HttpServletResponse.SC_OK);
            baseRequest.setHandled(true);
        }
        if (target.startsWith("/time-lapse")) {
            jsonMapper.writeValue(response.getWriter(), box.getTamelapseImages());
            response.setStatus(HttpServletResponse.SC_OK);
            baseRequest.setHandled(true);            
        }
        if (target.startsWith(PATH)) {
            String id = request.getParameter("id");
            if (id.equals("camButton")) {
                response.getWriter().print(box.makePhoto());
            } else {
                try {
                    box.toggleDevice(id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            response.setStatus(HttpServletResponse.SC_OK);
            baseRequest.setHandled(true);
        }

    }
}
