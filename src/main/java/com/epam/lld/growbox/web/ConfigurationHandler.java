package com.epam.lld.growbox.web;

import com.epam.lld.growbox.GrowBox;
import com.epam.lld.growbox.config.ConfigDto;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.BeanUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

/**
 *
 * @author Pavel_Vervenko
 */
public class ConfigurationHandler extends AbstractHandler {

    private final ConfigDto config;
    private final ObjectMapper jsonMapper;
    private final File configFile = new File("config.json");
    private final GrowBox box;

    public ConfigurationHandler(GrowBox box) {
        this.jsonMapper = new ObjectMapper();
        config = readConfigFromFile();
        this.box = box;
    }

    @Override
    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if (target.startsWith("/saveConfig")) {
            Map map = request.getParameterMap();
            try {
                BeanUtils.populate(config, map);
                jsonMapper.writeValue(configFile, config);
                box.reloadConfig(config);
            } catch (Throwable ex) {
                ex.printStackTrace();
            }
            response.setStatus(HttpServletResponse.SC_OK);
            baseRequest.setHandled(true);
        }
        if (target.startsWith("/getconfig")) {
            jsonMapper.writeValue(response.getWriter(), config);
            response.setStatus(HttpServletResponse.SC_OK);
            baseRequest.setHandled(true);
        }
    }

    public static ConfigDto readConfigFromFile() {
        try {
            ConfigDto readValue = (new ObjectMapper()).readValue(new File("config.json"), ConfigDto.class);
            return readValue != null ? readValue : new ConfigDto();
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
