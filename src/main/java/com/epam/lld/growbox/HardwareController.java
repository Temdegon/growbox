package com.epam.lld.growbox;

import java.io.File;

/**
 *
 * @author Pavel_Vervenko
 */
public interface HardwareController {
    void startFan();
    void stopFan();
    void startWatering();
    void stopWatering();
    void lightOn();
    void lightOff();
    float getTemprature();
    float getHumidity();
    File captureFrame();
    boolean isFanOn();
    public boolean isLightOn();
    public boolean isWateringOn();
}
