package com.epam.lld.growbox;

import com.epam.lld.growbox.sensor.Sensor;
import com.github.sarxos.webcam.Webcam;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author Pavel_Vervenko
 */
class PiHardwareController implements HardwareController {

    private static final Pin LIGHT_PIN = RaspiPin.GPIO_07;
    private static final Pin WATER_PUMP_PIN = RaspiPin.GPIO_03;
    private static final Pin FAN_PIN = RaspiPin.GPIO_00;
    private final GpioPinDigitalOutput light;
    private final GpioPinDigitalOutput water;
    private final GpioPinDigitalOutput fan;
    private float temp = 0f;
    private float hum = 0f;
    private Thread sensorReadingThread;
    private Webcam webcam;
    private final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    
    public PiHardwareController() {
        GpioController gpio = GpioFactory.getInstance();
        light = gpio.provisionDigitalOutputPin(LIGHT_PIN, "Light", PinState.LOW);
        light.setShutdownOptions(true, PinState.LOW, PinPullResistance.OFF);

        water = gpio.provisionDigitalOutputPin(WATER_PUMP_PIN, "Water", PinState.LOW);
        water.setShutdownOptions(true, PinState.LOW, PinPullResistance.OFF);

        fan = gpio.provisionDigitalOutputPin(FAN_PIN, "Fan", PinState.LOW);
        fan.setShutdownOptions(true, PinState.LOW, PinPullResistance.OFF);
        startSensorReading();
        initCam();
    }

    @Override
    public void startFan() {
        fan.high();
    }

    @Override
    public void stopFan() {
        fan.low();
    }

    @Override
    public void startWatering() {
        water.high();
    }

    @Override
    public void stopWatering() {
        water.low();
    }

    @Override
    public void lightOn() {
        light.high();
    }

    @Override
    public void lightOff() {
        light.low();
    }

    @Override
    public float getTemprature() {
        return temp;
    }

    @Override
    public float getHumidity() {
        return hum;
    }

    @Override
    public File captureFrame() {
        webcam.open();
        BufferedImage image = webcam.getImage();
        putDateTimeOnImage(image);
        File file = new File("cam/" + getFileName());
        try {
            ImageIO.write(image, "PNG", file);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("cam Error");
        } finally {
            webcam.close();
        }
        return file;
    }

    @Override
    public boolean isFanOn() {
        return fan.isHigh();
    }

    @Override
    public boolean isLightOn() {
        return light.isHigh();
    }

    @Override
    public boolean isWateringOn() {
        return water.isHigh();
    }

    private void startSensorReading() {
        sensorReadingThread = new Thread() {
            
            @Override
            public void run() {
                Sensor sensor = new Sensor();
                while(true) {
                    float[] sensData = sensor.readData();
                    temp = sensData[0];
                    hum = sensData[1];
                    try {
                        sleep(5000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(PiHardwareController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            
        };
        sensorReadingThread.start();
    }

    private void initCam() {
        webcam = Webcam.getDefault();
        Dimension[] viewSizes = webcam.getViewSizes();
        webcam.setViewSize(viewSizes[viewSizes.length - 1]);
        //webcam.open();
        System.out.println("cam ready");
    }

    private String getFileName() {
        return System.currentTimeMillis() + ".png";
    }

    private void putDateTimeOnImage(BufferedImage image) {
        Graphics2D gr = image.createGraphics();
        gr.setPaint(Color.RED);
        gr.setFont(new Font("Serif", Font.BOLD, 30));
        gr.drawString(sdf.format(new Date()), 50, image.getHeight() - 50);
        gr.dispose();
    }
}
