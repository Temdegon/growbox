package com.epam.lld.growbox;

import com.epam.lld.growbox.debug.FakeHardwareController;
import com.epam.lld.growbox.web.ConfigurationHandler;

public class App {

    public static GrowBox createGrowBox(boolean isRunningOnRasbery) {
        if (isRunningOnRasbery) {
            return new GrowBox(ConfigurationHandler.readConfigFromFile(), new PiHardwareController());
        }
        return new GrowBox(ConfigurationHandler.readConfigFromFile(), new FakeHardwareController());
    }

    public static boolean isRasbery() {
        return !System.getProperty("os.name").toLowerCase().startsWith("win");
    }
}
