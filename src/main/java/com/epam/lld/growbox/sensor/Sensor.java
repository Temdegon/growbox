package com.epam.lld.growbox.sensor;


/**
 *
 * @author Pavel
 */
public class Sensor {

    public static native float[] readData(int pin);

    static {
        System.loadLibrary("dhtreader");
    }

    public static void main(String[] args) {

        float[] readData = Sensor.readData(23);
        System.out.println("temp: " + readData[0]+ ", hum" + readData[1]);
    }
    
    public float[] readData() {
        float[] data = Sensor.readData(23);
        int stopCounter = 0;
        while (!isValid(data)) {
            System.out.println("Invalid sensor data in java. retry");
            stopCounter++;
            if (stopCounter > 10) {
                throw new RuntimeException("Sensor return invalid data 10 times:" + data[0] + ", " + data[1]);
            }
            data = Sensor.readData(23);
        }
        return data;
    }
    
    private boolean isValid(float[] data) {
        return data[0] > 0 && data[0] < 100 && data[1] > 0 && data[1] < 100;
    }
}
