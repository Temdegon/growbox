//  How to access GPIO registers from C-code on the Raspberry-Pi
//  Example program
//  15-January-2012
//  Dom and Gert
//


// Access from ARM Running Linux

#define BCM2708_PERI_BASE        0x20000000
#define GPIO_BASE                (BCM2708_PERI_BASE + 0x200000) /* GPIO controller */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <fcntl.h>
#include <assert.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <bcm2835.h>
#include <unistd.h>
#include "Sensor.h"

#define MAXTIMINGS 100

//#define DEBUG 

#define DHT11 11
#define DHT22 22
#define AM2302 22

jint JNI_OnLoad(JavaVM* vm, void* reserved) {
	bcm2835_init();
	return JNI_VERSION_1_6;
}


int readDHT(int type, int pin, float *temp_p, float *hum_p)
{
   //bcm2835_init();

int bits[250], data[100];
int bitidx = 0;


 int counter = 0;
  int laststate = HIGH;
  int j=0;

  // Set GPIO pin to output
  bcm2835_gpio_fsel(pin, BCM2835_GPIO_FSEL_OUTP);

  bcm2835_gpio_write(pin, HIGH);
  usleep(500000);  // 500 ms
  bcm2835_gpio_write(pin, LOW);
  usleep(20000);

  bcm2835_gpio_fsel(pin, BCM2835_GPIO_FSEL_INPT);

  data[0] = data[1] = data[2] = data[3] = data[4] = 0;

  // wait for pin to drop?
  while (bcm2835_gpio_lev(pin) == 1) {
    usleep(1);
  }

  // read data!
int i=0;
  for (i; i< MAXTIMINGS; i++) {
    counter = 0;
    while ( bcm2835_gpio_lev(pin) == laststate) {
	counter++;
	//nanosleep(1);		// overclocking might change this?
        if (counter == 1000)
	  break;
    }
    laststate = bcm2835_gpio_lev(pin);
    if (counter == 1000) break;
    bits[bitidx++] = counter;

    if ((i>3) && (i%2 == 0)) {
      // shove each bit into the storage bytes
      data[j/8] <<= 1;
      if (counter > 200)
        data[j/8] |= 1;
      j++;
    }
  }

//  printf("Data (%d): 0x%x 0x%x 0x%x 0x%x 0x%x\n", j, data[0], data[1], data[2], data[3], data[4]);

  if ((j >= 39) &&
      (data[4] == ((data[0] + data[1] + data[2] + data[3]) & 0xFF)) ) {
     // yay!
     if (type == DHT11) {
	//printf("Temp = %d *C, Hum = %d \%\n", data[2], data[0]);
	
                                *temp_p = (float)data[2];
                                *hum_p = (float)data[0];
     }
     if (type == DHT22) {
	float f, h;
	h = data[0] * 256 + data[1];
	h /= 10;

	f = (data[2] & 0x7F)* 256 + data[3];
        f /= 10.0;
        if (data[2] & 0x80)  f *= -1;
	printf("Temp =  %.1f *C, Hum = %.1f \%\n", f, h);
    }
    return 1;
  }

  return 0;
}
JNIEXPORT jfloatArray JNICALL Java_com_epam_lld_growbox_sensor_Sensor_readData
  (JNIEnv *env, jobject obj, jint gpio_pin) {
//printf("reading\n");
  jfloatArray j_result = (*env)->NewFloatArray(env, 2);
  jfloat result[2];
  float t, h;
  int pin = (int)gpio_pin;
  int rs =  readDHT(11, pin, &t, &h);
// printf("T = %f *C, H = %f \%\n", t, h);
  int errorCounter = 0;
  while(rs == 0) {
//printf("error! rs is %d\n", rs);
    errorCounter++;
    if (errorCounter > 10) {
//printf("too much errors\n");
      break;
    }
    rs = readDHT(11, pin, &t, &h);
//printf("T = %f *C, H = %f \%\n", t, h);

  }
//printf("function rs %d\n", rs);
  result[0] = (jfloat)t;
  result[1] = (jfloat)h;
//result[0] = 11;
//result[1] = 22;
  (*env)->SetFloatArrayRegion(env, j_result, 0, 2, result);
  return j_result;

}

