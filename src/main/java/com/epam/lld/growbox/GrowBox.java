package com.epam.lld.growbox;

import com.epam.lld.growbox.config.ConfigDto;
import com.epam.lld.growbox.config.DayLightSettings;
import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pavel_Vervenko
 */
public class GrowBox {

    private HardwareController controller;
    private boolean running;
    private Thread mainThread;
    private File lastPhoto;
    private final DecimalFormat df = new DecimalFormat("0.#");
    private ConfigDto config;
    private DayLightSettings dls;
    private Timer timer;
    private static final int VATERING_PERFORMANCE_G_PER_SEC = 20;
    private static final long MILLS_PER_DAY = 24 * 60 * 60 * 1000;
    

    public GrowBox(ConfigDto config, HardwareController controller) {
        this.controller = controller;
        this.config = config;
        initLastPhoto();
        initDevicesState();
        initTimers();
        initFanControlThread();
    }

    private void initLastPhoto() {
        File photoDir = new File("cam");
        if (photoDir.exists()) {
            List<File> listFiles = Arrays.asList(photoDir.listFiles());
            lastPhoto = Collections.max(listFiles);
        }
    }

    public List<String> getTamelapseImages() {
        List<String> listFiles = new ArrayList<String>();
        File photoDir = new File("cam");
        if (photoDir.exists()) {
            for (File f: photoDir.listFiles()) {
                listFiles.add(f.getName());
            }
            
        }
        java.util.Collections.sort(listFiles);
        return listFiles;
    }
    
    private boolean isRunning() {
        return running;
    }

    public void shutdown() {
        running = false;
        mainThread.interrupt();
    }

    public Map<String, String> getStatus() {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("light", String.valueOf(controller.isLightOn()));
        hashMap.put("fan", String.valueOf(controller.isFanOn()));
        hashMap.put("pump", String.valueOf(controller.isWateringOn()));
        hashMap.put("temp", df.format(controller.getTemprature()).replace(',', '.'));
        hashMap.put("hum", df.format(controller.getHumidity()).replace(',', '.'));
        hashMap.put("img", lastPhoto.getName());
        return hashMap;
    }

    public void toggleDevice(String device) {
        if (device.equals("light")) {
            System.out.println("toggling light");
            if (controller.isLightOn()) {
                controller.lightOff();
            } else {
                controller.lightOn();
            }
        }
        if (device.equals("fan")) {
            System.out.println("toggling fan");
            if (controller.isFanOn()) {
                controller.stopFan();
            } else {
                controller.startFan();
            }
        }
        if (device.equals("pump")) {
            System.out.println("toggling pump");
            if (controller.isWateringOn()) {
                controller.stopWatering();
            } else {
                controller.startWatering();
            }
        }
    }

    public String makePhoto() {
        System.out.println("capturing frame");
        lastPhoto = controller.captureFrame();
        System.out.println("Catptured " + lastPhoto.getName());
        return lastPhoto.getName();
    }

    private void initDevicesState() {
        dls = DayLightSettings.fromString(config.getDayLight());

    }

    private void initTimers() {
        System.out.println("init timers");
        if (timer != null) {
            timer.cancel();
        }
        timer = new Timer();

        timer.scheduleAtFixedRate(createLightOnTask(),
                dls.getStartTime(),
                MILLS_PER_DAY);
        timer.scheduleAtFixedRate(createLightOffTask(),
                dls.getEndTime(),
                MILLS_PER_DAY);

        for (Date date : getPhotoDates()) {
            timer.scheduleAtFixedRate(createPhotoTask(), date, MILLS_PER_DAY);
        }

        timer.scheduleAtFixedRate(createWateringTask(), getWateringTime(), getWateringInterval());
    }

    private List<Date> getPhotoDates() {
        ArrayList<Date> rs = new ArrayList<Date>();
        Date now = new Date();
        for (String s : config.getPhotoTime().split(",")) {
            Date date = DayLightSettings.getDateTime(s.trim());
            if (date.before(now)) {
                date = DayLightSettings.addToTime(date, Calendar.DAY_OF_MONTH, 1);
            }
            rs.add(date);
        }
        return rs;
    }

    private TimerTask createPhotoTask() {
        return new TimerTask() {
            @Override
            public void run() {
                makePhoto();
            }
        };
    }

    private TimerTask createLightOffTask() {
        return new TimerTask() {
            @Override
            public void run() {
                System.out.println("light off");
                controller.lightOff();
            }
        };
    }

    private Date getWateringTime() {
        Date date = DayLightSettings.getDateTime(config.getWateringTime().trim());
        if (date.before(new Date())) {
            date = DayLightSettings.addToTime(date, Calendar.DAY_OF_MONTH, 1);
        }
        return date;
    }

    private TimerTask createWateringTask() {
        return new TimerTask() {

            @Override
            public void run() {
                System.out.println("watering on");
                controller.startWatering();
                try {
                    Thread.sleep(calculateTimeForVolume());
                } catch (InterruptedException ex) {
                    Logger.getLogger(GrowBox.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    System.out.println("watering off");
                    controller.stopWatering();
                }
            }

        };
    }

    private long calculateTimeForVolume() {
        return config.getWateringVolume() / VATERING_PERFORMANCE_G_PER_SEC * 1000;
    }

    private TimerTask createLightOnTask() {
        return new TimerTask() {
            @Override
            public void run() {
                System.out.println("light on");
                controller.lightOn();
            }
        };
    }

    public void reloadConfig(ConfigDto config) {
        this.config = config;
        initDevicesState();
        initTimers();
    }

    private long getWateringInterval() {
        String wateringInterval = config.getWateringInterval();
        return MILLS_PER_DAY * Integer.parseInt(wateringInterval);
    }

    private void initFanControlThread() {
        mainThread = new Thread() {
            @Override
            public void run() {
                while (true) {
                    try {
                        if (controller.isFanOn()) {
                            if (controller.getTemprature() < config.getMaxTemp() &&
                                    controller.getHumidity() < config.getMaxHum()) {
                                controller.stopFan();
                            }
                        } else {
                            if (controller.getTemprature() > config.getMaxTemp() ||
                                    controller.getHumidity() > config.getMaxHum()) {
                                controller.startFan();
                            }
                        }
                        Thread.sleep(1000);
                    } catch (Exception ex) {
                        Logger.getLogger(GrowBox.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };
        mainThread.start();        
    }
}
