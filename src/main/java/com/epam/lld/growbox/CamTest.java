package com.epam.lld.growbox;

import com.github.sarxos.webcam.Webcam;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * Capture frame from camera and save it to test.png.
 * @author Pavel
 */
public class CamTest {

    public static void main(String[] args) throws IOException {
        System.out.println(Webcam.getDriver().getDevices());
        Webcam webcam = Webcam.getDefault();
        setMaxResolution(webcam);
        webcam.open();
        BufferedImage image = webcam.getImage();
        ImageIO.write(image, "PNG", new File("test.png"));
    }

    private static void setMaxResolution(Webcam webcam) {
        Dimension[] viewSizes = webcam.getViewSizes();
        webcam.setViewSize(viewSizes[viewSizes.length - 1]);
    }
}
