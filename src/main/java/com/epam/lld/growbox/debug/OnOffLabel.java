package com.epam.lld.growbox.debug;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JLabel;

/**
 *
 * @author Pavel_Vervenko
 */
public class OnOffLabel extends JLabel{
    private boolean on;

    public OnOffLabel() {
        super("OFF");
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        if (on) {
            setForeground(Color.GREEN);
            setText("ON");
        } else {
            setForeground(Color.RED);
            setText("OFF");            
        }
        super.paintComponent(g); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void setState(boolean state) {
        on = state;
    }
    
    public void on() {
        on = true;
        repaint();
    }
    
    public void off() {
        on = false;
        repaint();
    }
    
    public boolean isOn() {
        return on;
    }
}
