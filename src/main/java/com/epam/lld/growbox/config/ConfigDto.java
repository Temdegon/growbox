package com.epam.lld.growbox.config;

/**
 *
 * @author Pavel_Vervenko
 */
public class ConfigDto {

    String dayLight = "10:00-18:00";
    String wateringTime;
    Integer wateringVolume;
    String wateringInterval;
    Integer maxTemp;
    Integer maxHum;
    String photoTime;

    public String getDayLight() {
        return dayLight;
    }

    public void setDayLight(String dayLight) {
        this.dayLight = dayLight;
    }

    public String getWateringTime() {
        return wateringTime;
    }

    public void setWateringTime(String wateringTime) {
        this.wateringTime = wateringTime;
    }

    public Integer getWateringVolume() {
        return wateringVolume;
    }

    public void setWateringVolume(Integer wateringVolume) {
        this.wateringVolume = wateringVolume;
    }

    public String getWateringInterval() {
        return wateringInterval;
    }

    public void setWateringInterval(String wateringInterval) {
        this.wateringInterval = wateringInterval;
    }

    public Integer getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(Integer maxTemp) {
        this.maxTemp = maxTemp;
    }

    public Integer getMaxHum() {
        return maxHum;
    }

    public void setMaxHum(Integer maxHum) {
        this.maxHum = maxHum;
    }

    public String getPhotoTime() {
        return photoTime;
    }

    public void setPhotoTime(String photoTime) {
        this.photoTime = photoTime;
    }

    @Override
    public String toString() {
        return "ConfigDto{" + "dayLight=" + dayLight + ", wateringTime=" + wateringTime + ", wateringVolume=" + wateringVolume + ", wateringInterval=" + wateringInterval + ", maxTemp=" + maxTemp + ", maxHum=" + maxHum + ", photoTime=" + photoTime + '}';
    }

    
}
